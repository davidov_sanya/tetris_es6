import './styles/styles.css'
import { Game } from './core/Game';
import { SIZE_BUFFER_FIELD, SIZE_PLAY_FIELD } from './const/field.const';
import { Draw } from './core/Draw';
import { ModalWindow } from './modalWindows/modalWindow';
import { apiService } from './services/api.service';
import { StartModalWindow } from './modalWindows/start.modalWindow';
import { UpdateTopModalWindow } from './modalWindows/updateTop.modalWindow';
import { PauseModalWindow } from './modalWindows/pause.modalWindow';

new Draw('playField', SIZE_PLAY_FIELD);
new Draw('bufferField', SIZE_BUFFER_FIELD);

const startModalWindow = new StartModalWindow('startModalWindow');
const gameOverModalWindow = new ModalWindow('gameOverModalWindow');
const pauseModalWindow = new PauseModalWindow('pauseModalWindow');
const updateTopModalWindow = new UpdateTopModalWindow('updateTopModalWindow');

const btnStart = document.getElementById('buttonStart');
const btnRestart = document.getElementById('buttonRestart');
const btnUpdateTop = document.getElementById('buttonUpdateTop');

btnStart.addEventListener('click', buttonStart);
btnRestart.addEventListener('click', buttonRestart);

function buttonStart(){
  startModalWindow.hide();
  new Game();
}

function buttonRestart(){
  gameOverModalWindow.hide();
  new Game();
}

function updateProgress(event){
  const {level, lines, score} = event.detail;
  document.getElementById('level').innerText = level;
  document.getElementById('lines').innerText = lines;
  document.getElementById('score').innerText = score;
}

function updateTop(top, score){
  let name = document.getElementById('newChampion').value;
  if (name === '') {
    name = 'Anonymous';
  }
  const newChampion = {
    name,
    score
  };

  updateTopModalWindow.updateTop(top, newChampion);
  updateTopModalWindow.hide();
  startModalWindow.show();
  startModalWindow.showTop('local');
}

async function gameOver(event){
  const {score} = event.detail;
  const top = await apiService.fetchTop();

  localStorage.setItem('top', JSON.stringify(top));

  if (top[top.length - 1].score < score) {

    document.getElementById('recordScore').innerText = score;
    updateTopModalWindow.show();
    updateTopModalWindow.showTop('local');

    btnUpdateTop.addEventListener('click', () => {
      updateTop(top, score);
    });

  } else {
    document.getElementById('level').innerText = '1';
    document.getElementById('lines').innerText = '0';
    document.getElementById('score').innerText = '0';
    document.getElementById('finalScore').innerText = score;
    gameOverModalWindow.show();
    gameOverModalWindow.showTop('local');
  }

}

function pause(event){
  const {pause} = event.detail;
  if (pause) {
    pauseModalWindow.show();
  } else {
    pauseModalWindow.hide();
  }
}

document.addEventListener('updateProgress', event => {
  updateProgress(event)
});
document.addEventListener('gameOver', event => {
  gameOver(event)
});
document.addEventListener('pause', event => {
  pause(event)
});
