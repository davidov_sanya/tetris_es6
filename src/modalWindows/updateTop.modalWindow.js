import { ModalWindow } from './modalWindow';
import { apiService } from '../services/api.service';

export class UpdateTopModalWindow extends ModalWindow {
  constructor(id){
    super(id);
  }

  updateTop(top, newChampion){

    let i = 0;
    while (top[i].score >= newChampion.score) {
      i++;
    }

    top.splice(i, 0,newChampion);
    top.pop();

    localStorage.setItem('top', JSON.stringify(top));
    apiService.updateTop(top);
  }


}