import { apiService } from '../services/api.service';

export class ModalWindow {
  constructor(id){
    this.$el = document.getElementById(id);
    this.init()
  }

  init(){
  }

  onHide(){
  }

  async showTop(fromShow){
    let top = JSON.parse(localStorage.getItem('top'));
    if (fromShow === 'base' ){
      top = await apiService.fetchTop();
      localStorage.setItem('top', JSON.stringify(top));
    }


    let table = this.$el.getElementsByTagName('table')[0];

    while (table.firstChild) {
      table.removeChild(table.firstChild);
    }

    let tr = document.createElement("tr");
    let th1 = document.createElement("th");
    let th2 = document.createElement("th");
    let th3 = document.createElement("th");

    th1.appendChild(document.createTextNode(`#`));
    th2.appendChild(document.createTextNode(`Name`));
    th3.appendChild(document.createTextNode(`Score`));
    tr.appendChild(th1);
    tr.appendChild(th2);
    tr.appendChild(th3);
    table.appendChild(tr);

    top.map((key, i) => {
      let tr = document.createElement("tr");
      let td1 = document.createElement("td");
      let td2 = document.createElement("td");
      let td3 = document.createElement("td");

      td1.appendChild(document.createTextNode(`${i + 1}`));
      td2.appendChild(document.createTextNode(`${key.name}`));
      td3.appendChild(document.createTextNode(`${key.score}`));
      tr.appendChild(td1);
      tr.appendChild(td2);
      tr.appendChild(td3);


      table.appendChild(tr);
    });

    //return top;
  }

  hide(){
    this.$el.classList.add('hide');
    this.onHide();
  }

  show(){
    this.$el.classList.remove('hide');


    //return this.onShow();
  }

}