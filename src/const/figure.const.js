const g = {isRotatable: true, data: [[1, 1], [1, 0], [0, 0], [-1, 0]]};
const g2 = {isRotatable: true, data: [[1, -1], [1, 0], [0, 0], [-1, 0]]};
const z = {isRotatable: true, data: [[-1, 0], [0, 0], [0, -1], [1, -1]]};
const z2 = {isRotatable: true, data: [[-1, -1], [0, -1], [0, 0], [1, 0]]};
const t = {isRotatable: true, data: [[-1, 0], [0, 0], [1, 0], [0, 1]]};
const line = {isRotatable: true, data: [[0, 2], [0, 1], [0, 0], [0, -1]]};
const square = {isRotatable: false, data: [[1, 1], [1, 0], [0, 0], [0, 1]]};

export const ALL_FIGURE = [g, g2, z, z2, t, line, square];

export const ARRAY_COLOR = ["#DC143C","#FF0000","#8B0000","#FF1493","#FF4500","#FFA500",
  "#8A2BE2","#7FFF00","#00FA9A","#66CDAA","#00FFFF","#00008B"];