export const SCALE = 26;

export const EMPTY_CELL = '#ffffff';

export const SIZE_PLAY_FIELD = {
  width: 10,
  height: 20,
};

export const SIZE_BUFFER_FIELD = {
  width: 4,
  height: 4,
};