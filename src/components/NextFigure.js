import { Figure } from '../core/Figure';
import { ALL_FIGURE, ARRAY_COLOR } from '../const/figure.const';
import { SIZE_BUFFER_FIELD } from '../const/field.const';

export class NextFigure extends Figure{
  constructor(){
    super('bufferField', SIZE_BUFFER_FIELD);
    this.createFigure();
    this.searchMinMax();
    this.setPosition(0-this.minMaxXY.minX, this.minMaxXY.maxY);
    this.draw();
  }

  createFigure() {
    let figure = {...ALL_FIGURE[Math.floor(Math.random() * 7)]};

    this.data = [...figure.data];
    figure.data.forEach((value, i)=>{
      this.data[i] = [...value];
    });

    this.isRotatable = figure.isRotatable;
    this.color = ARRAY_COLOR[Math.floor(Math.random() * ARRAY_COLOR.length)];
    //this.position = {x: 0, y: 0};

    let temp = Math.floor(Math.random() * 3);
    for (let i = 0; i < temp; i++) {
      this.rotate();
    }
  }


}