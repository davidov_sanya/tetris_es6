import { Figure } from '../core/Figure';
import { SIZE_PLAY_FIELD } from '../const/field.const';
import { NextFigure } from './NextFigure';

export class FocusFigure extends Figure {
  constructor(){
    super('playField', SIZE_PLAY_FIELD);
    this.nextFigure = new NextFigure();
    this.getNextFigure();
    this.setStartPosition();

  }

  setStartPosition(){
    this.setPosition(SIZE_PLAY_FIELD.width / 2 + this.minMaxXY.minX, this.minMaxXY.maxY);
  }

  getNextFigure(){
    const {data, isRotatable, color} = this.nextFigure;
    this.data = data;
    this.isRotatable = isRotatable;
    this.color = color;
    this.searchMinMax();
    this.setStartPosition();
    this.nextFigure = new NextFigure();
    this.draw();
  }

}
