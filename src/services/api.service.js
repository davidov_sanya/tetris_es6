class ApiService {
  constructor(baseUrl){
    this.url = baseUrl;
  }

  async updateTop(top){
    try {
      const request = new Request(`${this.url}/top.json`, {
        method: 'put',
        body: JSON.stringify(top)
      });

      const response = await fetch(request);
      return await response;

    } catch (e) {
      console.log('Error',e);
    }
  }


  async fetchTop(){
    try {
      const request = new Request(`${this.url}/top.json`, {
        method: 'get'
      });

      const response = await fetch(request);
      const top = await response.json();

      top.sort((a, b) => {
        if (a.score < b.score) return 1;
        if (a.score > b.score) return -1;
      });

      return top;

    } catch (e) {
      console.log('Error',e);
    }
  }
}

export const apiService = new ApiService('https://tetris-383da.firebaseio.com');