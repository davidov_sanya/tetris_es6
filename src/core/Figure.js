import { Draw } from './Draw';

export class Figure extends Draw{
  constructor(id, size) {
    super(id, size)
  }

  setPosition(x, y) {
    this.position = {x, y};
  }

  rotate() {
    const {data, isRotatable} = this;

    if (isRotatable === false) return;

    let temp;
    data.forEach((value) => {
      temp = value[0];
      value[0] = value[1];
      value[1] = -temp;
    });

    this.data = data;
    this.searchMinMax();
  }


  searchMinMax() {

    const {data} = this;
    let minX = 0, minY = 0, maxX = 0, maxY = 0;

    data.forEach((value) => {
      if (minX > value[0]) minX = value[0];
      if (maxX < value[0]) maxX = value[0];
      if (minY > value[1]) minY = value[1];
      if (maxY < value[1]) maxY = value[1];
    });

    this.minMaxXY = {minX, minY, maxX, maxY};
  }


}

