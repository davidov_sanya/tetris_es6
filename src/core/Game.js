import { Field } from './Field';


export class Game {
  constructor(){
    this.pause = false;
    this.score = 0;
    this.level = 1;
    this.counterUpLevel = 0;
    this.speedLevel = 900;
    this.field = new Field();
    this.start();

    this.onKeyDown = this.moveRect.bind(this);


    document.addEventListener('keydown', this.onKeyDown);
  }

  gameOver(){
    clearTimeout(this.timer);
    document.removeEventListener('keydown', this.onKeyDown);

    let event = new CustomEvent('gameOver', {
      'detail': {
        score: this.score
      }
    });

    this.pause = true;
    document.dispatchEvent(event);
  }

  updateProgress(countDeleteLine){
    switch ( countDeleteLine ) {
      case 2:
        this.score += 300;
        break;
      case 3:
        this.score += 700;
        break;
      case 4:
        this.score += 1500;
        break;
      default:
        this.score += 100;
    }

    this.score += 10 * this.level;
    this.counterUpLevel += countDeleteLine;

    if (this.counterUpLevel >= 10) {
      this.level++;
      this.counterUpLevel = 0;

      if (this.level > 5) {
        this.speedLevel -= 50;
      } else if (this.level > 7) {
        this.speedLevel -= 25;
      } else
        this.speedLevel -= 100;
    }

    let event = new CustomEvent('updateProgress', {
      'detail': {
        level: this.level,
        lines: this.counterUpLevel,
        score: this.score
      }
    });

    document.dispatchEvent(event);
  }


  moveDown(){
    if (this.pause) return;
    const result = this.field.moveDown();
    switch ( result.status ) {

      case 'gameOver':
        this.gameOver();
        break;

      case 'updateProgress':
        this.updateProgress(result.countDeleteLine);
        break;

      default:
        return;
    }
  }

  moveRect(e){
    const {field} = this;
    if (this.pause && e.keyCode !== 32) return;

    if (e.keyCode === 32) {
      this.pause = !this.pause;

      document.dispatchEvent(new CustomEvent('pause',
        {
          'detail': {
            pause: this.pause
          }
        }
      ));


      if (this.pause) {
        clearTimeout(this.timer);
        return;
      } else {
        this.start();
      }
    }

    if (e.keyCode === 38) {
      field.rotateFigure();
    }

    if (e.keyCode === 37) {
      field.moveLeft();
    }

    if (e.keyCode === 39) {
      field.moveRight();
    }

    if (e.keyCode === 40) {
      this.moveDown();
    }

  };


  start(){
    this.timer = () => {
      if (!this.pause) {
        this.moveDown();
        setTimeout(this.timer, this.speedLevel);
      }
    };
    setTimeout(this.timer, this.speedLevel);
  }


}