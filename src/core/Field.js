import { Draw } from './Draw';
import { EMPTY_CELL, SCALE, SIZE_PLAY_FIELD } from '../const/field.const';
import { FocusFigure } from '../components/FocusFigure';

export class Field extends Draw {
  constructor(){
    super('playField', SIZE_PLAY_FIELD);
    this.initEmptyLine();
    this.initArrayField();
    this.figure = new FocusFigure();
  }


  initEmptyLine(){
    this.emptyLine = [];
    for ( let i = 0; i < SIZE_PLAY_FIELD.width; i++ ) {
      this.emptyLine[i] = EMPTY_CELL;
    }
  }

  initArrayField(){
    this.arrayField = [];
    for ( let i = 0; i < SIZE_PLAY_FIELD.height; i++ ) {
      this.arrayField[i] = [...this.emptyLine];
    }
  }


  fillField(){
    const {figure} = this;
    const {position, data, color} = figure;

    data.forEach((value) => {
      this.arrayField[position.y - value[1]] [position.x + value[0]] = color;
    });

  }

  checkReverse(){
    const {width, height} = SIZE_PLAY_FIELD;
    const {figure, arrayField} = this;
    const {position, data, minMaxXY} = figure;
    const {minY, maxY, minX, maxX} = minMaxXY;

    if (position.x + minY < 0
      || position.x + maxY > width - 1
      || position.y + minX < 0
      || position.y + maxX > height - 1
    ) return false;

    return !data.some((value) => (
      arrayField [position.y + value[0]] [position.x + value[1]] !== EMPTY_CELL
    ));
  }


  rotateFigure(){
    if (!this.checkReverse()) return;

    this.draw();
    this.figure.rotate();
    this.figure.draw();
  }

  checkMove(move){
    const {figure, arrayField} = this;
    const {position, data, minMaxXY} = figure;
    const {minX, maxX} = minMaxXY;

    if (( ( position.x + minX ) === 0 ) && ( move === -1 )) return false;
    if (( ( position.x + maxX ) === SIZE_PLAY_FIELD.width - 1 ) && ( move === 1 )) return false;

    return !data.some((value) => (
      arrayField[position.y - value[1]] [position.x + value[0] + move] !== EMPTY_CELL
    ));

  }

  moveLeft(){
    const {x, y} = this.figure.position;
    if (this.checkMove(-1)) {
      this.draw();
      this.figure.setPosition(x - 1, y);
      this.figure.draw();
    }

  }

  moveRight(){
    const {x, y} = this.figure.position;
    if (this.checkMove(1)) {
      this.draw();
      this.figure.setPosition(x + 1, y);
      this.figure.draw();
    }

  }

  checkNextLine(){
    const {height} = SIZE_PLAY_FIELD;
    const {figure, arrayField} = this;
    const {position, data, minMaxXY} = figure;
    const {minY} = minMaxXY;

    if (( position.y - minY ) >= height - 1) return true;

    return data.some((value) => (
      arrayField[position.y - value[1] + 1] [position.x + value[0]] !== EMPTY_CELL
    ));

  }

   deleteFullLine(){
    let count = 0;

    this.arrayField.forEach((line, i) => {
      if (!line.some((value) => value === EMPTY_CELL)) {
        this.arrayField.splice(i, 1);
        this.arrayField.unshift([...this.emptyLine]);
        count++;
      }
    });
    this.draw();
    this.figure.draw();
    return count;
  }

  moveDown(){
    if (this.checkNextLine()) {

      this.fillField();
      this.figure.getNextFigure();

      const countDeleteLine = this.deleteFullLine();
      if (countDeleteLine !== 0) {
        return {
          status: 'updateProgress',
          countDeleteLine: countDeleteLine
        }
      }


      this.figure.setPosition(this.figure.position.x, this.figure.position.y - 1);
      if (this.checkNextLine()) {
        return {
          status: 'gameOver'
        }
      }

    }

    this.draw();
    this.figure.setPosition(this.figure.position.x, this.figure.position.y + 1);
    this.figure.draw();
    return {
      status: 'moveDown'
    }
  }


  draw(){
    const {ctx} = this;

    this.arrayField.forEach((line, i) => {
      line.forEach((color, j) => {
        ctx.fillStyle = color;
        ctx.fillRect(j * SCALE + 1, i * SCALE + 1, SCALE - 1, SCALE - 1);
      })
    });
  }


}