import { EMPTY_CELL, SCALE } from '../const/field.const';

export class Draw {
  constructor(id, size){
    this.$el = document.getElementById(id);
    this.$el.width = size.width * SCALE + 1;
    this.$el.height = size.height * SCALE + 1;
    this.ctx = this.$el.getContext('2d');
    this.init();
  }

  // Render grid for field
  init(){
    const {ctx} = this;
    ctx.width = this.$el.width;
    ctx.height = this.$el.height;
    ctx.fillStyle = EMPTY_CELL;
    ctx.fillRect(0.5, 0.5, ctx.width - 1, ctx.height - 1);

    ctx.strokeStyle = '#DCDCDC';
    for ( let i = 1; i < ctx.width / SCALE; i++ ) {
      ctx.moveTo(i * SCALE + 0.5, 0);
      ctx.lineTo(i * SCALE + 0.5, ctx.height);
    }
    for ( let i = 1; i < ctx.height / SCALE; i++ ) {
      ctx.moveTo(0, i * SCALE + 0.5);
      ctx.lineTo(ctx.width, i * SCALE + 0.5);
    }
    ctx.stroke();

    ctx.strokeStyle = 'Black';
    ctx.strokeRect(0.5, 0.5, ctx.width - 1, ctx.height - 1);
  }

  // Render figure
  draw(){
    const {ctx, data, position, color} = this;
    const {x, y} = position;
    data.map((key, i) => {
      ctx.fillStyle = color;
      ctx.fillRect(x * SCALE + data[i][0] * SCALE + 1, y * SCALE - data[i][1] * SCALE + 1, SCALE - 1, SCALE - 1);
    });
  }


}
